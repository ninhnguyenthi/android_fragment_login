package com.example.loginapp;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Build;
import android.os.Bundle;

import androidx.fragment.app.Fragment;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

public class LoginFragment extends Fragment {
    public static final String USERNAME = "nina";
    public static final String PASSWORD = "12345";

    Button btLogin;
    EditText edUsername, edPassword;
    String userName, passWord;

    SharedPreferences sharedPreferences;
    SharedPreferences.Editor editor;

    public LoginFragment() {
        // Required empty public constructor
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_login, container, false);
        edUsername = view.findViewById(R.id.ed_username);
        edPassword = view.findViewById(R.id.ed_password);
        btLogin = view.findViewById(R.id.bt_login);

        //initialize the ui widgets
        initUI();

        return view;
    }

    /**
     * get data from input and validateCredentials
     */
    public void initUI() {
        btLogin.setOnClickListener(v -> {
            //get data from user input
            userName = edUsername.getText().toString();
            passWord = edPassword.getText().toString();

            //validate credentials
            validateCredentials(userName, passWord);
        });
    }

    /**
     * using SharePreference
     */
    private void usingPreferences() {
        if (getActivity() != null) {
            sharedPreferences = getActivity().getSharedPreferences("useFile", Context.MODE_PRIVATE);
        }
        editor = sharedPreferences.edit();
        editor.putString("name", USERNAME);
        editor.putString("pass", PASSWORD);
        editor.putBoolean("active", true);
        editor.apply();
    }

    /**
     * Validate user && password
     */
    public void validateCredentials(String userName, String passWord) {
        if (userName.equals(USERNAME) && passWord.equals(PASSWORD)) {
            usingPreferences();
            showToastMessage("Login successfully!");

            Intent intent = new Intent(getActivity(), MainActivity.class);
            intent.putExtra(USERNAME, edUsername.getText().toString());
            startActivity(intent);
            if (getActivity() != null) {
                getActivity().finish();
            }
        } else {
            showToastMessage("Failed to login with username:" + userName);
        }
    }

    /**
     * Validate user && password
     * show the toast message
     *
     * @param message String message to display
     */
    private void showToastMessage(String message) {
        Toast.makeText(getContext(), message, Toast.LENGTH_SHORT).show();
    }

    /**
     * get data from useFile
     */
    private String getData() {
        if (getActivity() != null) {
            sharedPreferences = getActivity().getSharedPreferences("useFile", Context.MODE_PRIVATE);
        }
        return sharedPreferences.getString("name", "");
    }


}
