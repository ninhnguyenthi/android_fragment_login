package com.example.loginapp;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;

import androidx.fragment.app.Fragment;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

/**
 * A simple {@link Fragment} subclass.
 * create an instance of this fragment.
 */
public class MainFragment extends Fragment {

    TextView tvUsername;
    Button btLogout;
    SharedPreferences sharedPreferences;
    SharedPreferences.Editor editor;

    public MainFragment() {
        // Required empty public constructor
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_main, container, false);

        tvUsername = view.findViewById(R.id.tv_name);
        btLogout = view.findViewById(R.id.bt_logout);

        tvUsername.setText(getUsername());

        btLogout.setOnClickListener(v -> {
            Toast.makeText(getActivity(), "Logout success !", Toast.LENGTH_SHORT).show();
            logout();
            startActivity(new Intent(getActivity(), LoginActivity.class));

            if (getActivity() != null) {
                getActivity().finish();
            }

        });

        return view;
    }

    /**
     * get data from useFile
     */
    private String getUsername() {
        if (getActivity() != null) {
            sharedPreferences = getActivity().getSharedPreferences("useFile", Context.MODE_PRIVATE);
        }
        return sharedPreferences.getString("name", "Error");
    }

    /**
     * check logout condition
     */
    public void logout() {
        if (getActivity() != null) {
            sharedPreferences = getActivity().getSharedPreferences("useFile", Context.MODE_PRIVATE);
        }
        editor = sharedPreferences.edit();
        editor.putBoolean("active", false);
        editor.apply();
    }

}