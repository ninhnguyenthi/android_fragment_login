package com.example.loginapp;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;

public class LoginActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_login);

        if (getStateLogin()) {
            startActivity(new Intent(this, MainActivity.class));
            finish();
            return;
        }
        getSupportFragmentManager().beginTransaction().add(R.id.fragment_container, new LoginFragment()).commit();
    }

    public boolean getStateLogin() {
        SharedPreferences sharedPreferences = getSharedPreferences("useFile", Context.MODE_PRIVATE);
        return sharedPreferences.getBoolean("active", false);
    }


}